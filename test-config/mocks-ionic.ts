import { ElementRef }      from '@angular/core';

export class PlatformMock {
  public ready(): Promise<{String}> {
    return new Promise((resolve) => {
      resolve('READY');
    });
  }

  public getQueryParam() {
    return true;
  }

  public registerBackButtonAction(fn: Function, priority?: number): Function {
    return (() => true);
  }

  public hasFocus(ele: HTMLElement): boolean {
    return true;
  }

  public doc(): HTMLDocument {
    return document;
  }

  public is(): boolean {
    return true;
  }

  public getElementComputedStyle(container: any): any {
    return {
      paddingLeft: '10',
      paddingTop: '10',
      paddingRight: '10',
      paddingBottom: '10',
    };
  }

  public onResize(callback: any) {
    return callback;
  }

  public registerListener(ele: any, eventName: string, callback: any): Function {
    return (() => true);
  }

  public win(): Window {
    return window;
  }

  public raf(callback: any): number {
    return 1;
  }

  public timeout(callback: any, timer: number): any {
    return setTimeout(callback, timer);
  }

  public cancelTimeout(id: any) {
    // do nothing
  }

  public getActiveElement(): any {
    return document['activeElement'];
  }
}

export class AlertMock {

  public create(): any {
    let rtn: Object = {};
    rtn['present'] = (() => true);
    return rtn;
  }

  // function actually on the AlertClass (not AlertController), but using these interchangably for now
  public dismiss(): Promise<{}> {
    return new Promise(function(resolve: Function): void {
      resolve();
    });
  }
}

export class ToastMock {

  public create(): any {
    let rtn: Object = {};
    rtn['present'] = (() => true);
    return rtn;
  }
}

export class ConfigMock {

  public get(): any {
    return '';
  }

  public getBoolean(): boolean {
    return true;
  }

  public getNumber(): number {
    return 1;
  }

  public setTransition(): void {
    return;
  }
}

export class FormMock {
  public register(): any {
    return true;
  }
}

export class NavMock {

  public pop(): any {
    return new Promise(function(resolve: Function): void {
      resolve();
    });
  }

  public push(): any {
    return new Promise(function(resolve: Function): void {
      resolve();
    });
  }

  public getActive(): any {
    return {
      'instance': {
        'model': 'something',
      },
    };
  }

  public setRoot(): any {
    return true;
  }

  public popToRoot(): any {
    return true;
  }
}

export class SplashMock {

  public hide() {
    return Promise.resolve(true);
  }
}

export class StatusMock {

  public styleDefault() {
    return Promise.resolve(true);
  }
}

export class StorageMock {

  public get(key: string): Promise<{}> {
    return new Promise((resolve: Function) => {
      resolve({});
    });
  }

  public set(key: string, value: string): Promise<{}> {
    return new Promise((resolve: Function) => {
      resolve({key: key, value: value});
    });
  }

  public remove(key: string): Promise<{}> {
    return new Promise((resolve: Function) => {
      resolve({key: key});
    });
  }

  public query(): Promise<{ res: { rows: Array<{}> }}> {
    return new Promise((resolve) => {
      resolve({
        res: {
          rows: [{}]
        }
      });
    });
  }
}

export class MenuMock {
  public close(): any {
    return new Promise((resolve: Function) => {
      resolve();
    });
  }
}

export class AppMock {

  public getActiveNav(): NavMock {
    return new NavMock();
  }
}

export class NavParamsMock {
  static returnParams: any = {};

  public get(key): any {
    if (NavParamsMock.returnParams[key]) {
       return NavParamsMock.returnParams[key];
    }
    return 'No Params of ' + key + ' was supplied. Use NavParamsMock.setParams('+ key + ',value) to set it.';
  }

  static setParams(key,value){
    NavParamsMock.returnParams[key] = value;
  }
}
export class MockElementRef implements ElementRef {
  nativeElement: any;
  constructor(ele: any) {
    this.nativeElement = ele;
  }
}

export class MockElement {
  children: any[] = [];
  classList = new ClassList();
  attributes: { [name: string]: any } = {};
  style: { [property: string]: any } = {};
  nodeName: string = 'ION-MOCK';

  clientWidth = 0;
  clientHeight = 0;
  clientTop = 0;
  clientLeft = 0;
  offsetWidth = 0;
  offsetHeight = 0;
  offsetTop = 0;
  offsetLeft = 0;
  scrollTop = 0;
  scrollHeight = 0;

  get className() {
    return this.classList.classes.join(' ');
  }

  set className(val: string) {
    this.classList.classes = val.split(' ');
  }

  hasAttribute(name: string) {
    return !!this.attributes[name];
  }

  getAttribute(name: string) {
    return this.attributes[name];
  }

  setAttribute(name: string, val: any) {
    this.attributes[name] = val;
  }

  addEventListener(type: string, listener: Function, options?: any) { }

  removeEventListener(type: string, listener: Function, options?: any) { }

  removeAttribute(name: string) {
    delete this.attributes[name];
  }
}
export class ClassList {
  classes: string[] = [];
  add(className: string) {
    if (!this.contains(className)) {
      this.classes.push(className);
    }
  }
  remove(className: string) {
    const index = this.classes.indexOf(className);
    if (index > -1) {
      this.classes.splice(index, 1);
    }
  }
  toggle(className: string) {
    if (this.contains(className)) {
      this.remove(className);
    } else {
      this.add(className);
    }
  }
  contains(className: string) {
    return this.classes.indexOf(className) > -1;
  }
}
export class MockRenderer {
  setElementAttribute(renderElement: MockElement, name: string, val: any) {
    if (name === null) {
      renderElement.removeAttribute(name);
    } else {
      renderElement.setAttribute(name, val);
    }
  }
  setElementClass(renderElement: MockElement, className: string, isAdd: boolean) {
    if (isAdd) {
      renderElement.classList.add(className);
    } else {
      renderElement.classList.remove(className);
    }
  }
  setElementStyle(renderElement: MockElement, styleName: string, styleValue: string) {
    renderElement.style[styleName] = styleValue;
  }
}
export class HapticMock {
  public available(): boolean {
    return false;
  }
  public gestureSelectionStart(): void {

  }
  public gestureSelectionEnd(): void {

  }
  public gestureSelectionChanged(): void {

  }
}

