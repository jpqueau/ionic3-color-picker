# Color Picker Component for Ionic 3
download or clone the application from:
https://jpqueau@bitbucket.org/jpqueau/ionic3-color-picker.git

## Setup

```bash
$ sudo npm install -g ionic@latest cordova
$ sudo git clone https://jpqueau@bitbucket.org/jpqueau/ionic3-color-picker.git
$ cd ionic3-color-picker
$ sudo npm install
```


Then, to run it,
$ sudo npm start   or ionic serve

To run it on cordova platforms  

```bash
$ ionic cordova platform add ios
$ ionic cordova run ios
```

Substitute ios for android if not on a Mac.


## Testing
To run the Unit Tests
$ sudo npm test
and then Control-C to exit the test at the end

To run the End-to-End tests
$ sudo npm run e2e


