import { browser, element, by, ElementFinder, ElementArrayFinder } from 'protractor';

describe('ColorPicker', () => {

  beforeEach(() => {
    browser.get('');
  });

  it('should have a card element ', () => {
    let card: ElementFinder = element(by.css('ion-card'));
    browser.isElementPresent(card).then(value => {
      expect(value).toBe(true);
    });
  });

  it('should have a Color button', () => {
    let button: ElementFinder = element(by.id('b-colorpicker'));
    browser.isElementPresent(button).then(value => {
      expect(value).toBe(true);
      button.getText().then(value => {
        expect(value).toEqual('Color');
      });
   });
  });

  it('should open rgb colorpicker', () => {
    let button: ElementFinder = element(by.id('b-colorpicker'));
    button.click().then(() => {
      browser.driver.sleep(1000); // wait for the animation
      let h2: ElementFinder = element(by.id('d-colorpicker'));
      h2.getText().then(text => {
        expect(text).toEqual('Choose a color');
      });
      let cols: ElementArrayFinder = element.all(by.css('ion-col'));
      cols.count().then((data) => {
        expect(data).toEqual(4);
      });
      cols.get(2).getText().then((data) => {
        let hexcolor: string = '#' + data.split('#')[1];
        expect(hexcolor).toEqual('#ffffff');
      });
      cols.get(3).getText().then((data) => {
        let hexselcolor: string = '#' + data.split('#')[1];
        expect(hexselcolor).toEqual('#ffffff');
      });
    });
  });

  it('should get back to Color button when pressing OK button on Choose a Color', () => {
    let button: ElementFinder = element(by.id('b-colorpicker'));
    button.click().then(() => {
      browser.driver.sleep(1000); // wait for the animation
      let h2: ElementFinder = element(by.id('d-colorpicker'));
      h2.getText().then(text => {
        expect(text).toEqual('Choose a color');
      });
      let cols: ElementArrayFinder = element.all(by.css('ion-col'));
      cols.count().then((data) => {
        expect(data).toEqual(4);
      });
      cols.get(2).getText().then((data) => {
        let hexcolor: string = '#' + data.split('#')[1];
        expect(hexcolor).toEqual('#ffffff');
      });
      cols.get(3).getText().then((data) => {
        let hexselcolor: string = '#' + data.split('#')[1];
        expect(hexselcolor).toEqual('#ffffff');
      });
      let buttonValid: ElementFinder = element(by.id('valid'));
      buttonValid.click().then(() => {
        browser.driver.sleep(1000); // wait for the animation
        let buttonColor: ElementFinder = element(by.id('b-colorpicker'));
        browser.isElementPresent(buttonColor).then(value => {
          expect(value).toBe(true);
          buttonColor.getText().then(text => {
            expect(text).toEqual('Color');
          });
        });
      });
    });
  });

  it('should get red color', () => {
    let button: ElementFinder = element(by.id('b-colorpicker'));
    button.click().then(() => {
      browser.driver.sleep(1000); // wait for the animation
      let sliders: ElementArrayFinder = element.all(by.className('range-knob-handle'));
      sliders.count().then((data) => {
        expect(data).toEqual(3);
      });
      // move the green slider to 0%
      setSliderPos(sliders.get(1), 100 - 0);
      browser.driver.sleep(1000); // wait for the animation
      // move the blue slider to 0%
      setSliderPos(sliders.get(2), 100 - 0);
      browser.driver.sleep(1000); // wait for the animation
      let cols: ElementArrayFinder = element.all(by.css('ion-col'));
      cols.get(2).getText().then((data) => {
        let hexcolor: string = '#' + data.split('#')[1];
        expect(hexcolor).toEqual('#ff0000');
      });
      cols.get(3).getText().then((data) => {
        let hexselcolor: string = '#' + data.split('#')[1];
        expect(hexselcolor).toEqual('#ff0000');
      });
    });
  });

  it('should get green color', () => {
    let button: ElementFinder = element(by.id('b-colorpicker'));
    button.click().then(() => {
      browser.driver.sleep(1000); // wait for the animation
      let sliders: ElementArrayFinder = element.all(by.className('range-knob-handle'));
      sliders.count().then((data) => {
        expect(data).toEqual(3);
      });
      // move the red slider to 0%
      setSliderPos(sliders.get(0), 100 - 0);
      browser.driver.sleep(1000); // wait for the animation
      // move the blue slider to 0%
      setSliderPos(sliders.get(2), 100 - 0);
      browser.driver.sleep(1000); // wait for the animation
      let cols: ElementArrayFinder = element.all(by.css('ion-col'));
      cols.get(2).getText().then((data) => {
        let hexcolor: string = '#' + data.split('#')[1];
        expect(hexcolor).toEqual('#00ff00');
      });
      cols.get(3).getText().then((data) => {
        let hexselcolor: string = '#' + data.split('#')[1];
        expect(hexselcolor).toEqual('#00ff00');
      });
    });
  });

  it('should get blue color', () => {
    let button: ElementFinder = element(by.id('b-colorpicker'));
    button.click().then(() => {
      browser.driver.sleep(1000); // wait for the animation
      let sliders: ElementArrayFinder = element.all(by.className('range-knob-handle'));
      sliders.count().then((data) => {
        expect(data).toEqual(3);
      });
      // move the red slider to 0%
      setSliderPos(sliders.get(0), 100 - 0);
      browser.driver.sleep(1000); // wait for the animation
      // move the green slider to 0%
      setSliderPos(sliders.get(1), 100 - 0);
      browser.driver.sleep(1000); // wait for the animation
      let cols: ElementArrayFinder = element.all(by.css('ion-col'));
      cols.get(2).getText().then((data) => {
        let hexcolor: string = '#' + data.split('#')[1];
        expect(hexcolor).toEqual('#0000ff');
      });
      cols.get(3).getText().then((data) => {
        let hexselcolor: string = '#' + data.split('#')[1];
        expect(hexselcolor).toEqual('#0000ff');
      });
    });
  });

  it('should get 25%R 50%G 75%B color', () => {
    let button: ElementFinder = element(by.id('b-colorpicker'));
    button.click().then(() => {
      browser.driver.sleep(1000); // wait for the animation
      let sliders: ElementArrayFinder = element.all(by.className('range-knob-handle'));
      sliders.count().then((data) => {
        expect(data).toEqual(3);
      });
      // move the red slider to 25%
      setSliderPos(sliders.get(0), 100 - 25);
      browser.driver.sleep(1000); // wait for the animation
      // move the green slider to 50%
      setSliderPos(sliders.get(1), 100 - 50);
      browser.driver.sleep(1000); // wait for the animation
      // move the green slider to 75%
      setSliderPos(sliders.get(2), 100 - 75);
      browser.driver.sleep(1000); // wait for the animation
      let cols: ElementArrayFinder = element.all(by.css('ion-col'));
      cols.get(2).getText().then((data) => {
        let hexcolor: string = '#' + data.split('#')[1];
        expect(hexcolor).toEqual('#3f7fbf');
      });
      cols.get(3).getText().then((data) => {
        let hexselcolor: string = '#' + data.split('#')[1];
        expect(hexselcolor).toEqual('#3f7fbf');
      });
    });
  });

  it('should get 25%R 50%G 75%B ligthen 25% color', () => {
    let button: ElementFinder = element(by.id('b-colorpicker'));
    button.click().then(() => {
      browser.driver.sleep(1000); // wait for the animation
      let sliders: ElementArrayFinder = element.all(by.className('range-knob-handle'));
      sliders.count().then((data) => {
        expect(data).toEqual(3);
      });
      // move the red slider to 25%
      setSliderPos(sliders.get(0), 100 - 25);
      browser.driver.sleep(1000); // wait for the animation
      // move the green slider to 50%
      setSliderPos(sliders.get(1), 100 - 50);
      browser.driver.sleep(1000); // wait for the animation
      // move the green slider to 75%
      setSliderPos(sliders.get(2), 100 - 75);
      browser.driver.sleep(1000); // wait for the animation
      let buttonModif: ElementFinder = element(by.id('modif'));
      buttonModif.click().then(() => {
        browser.driver.sleep(1000); // wait for the animation
        let h2: ElementFinder = element(by.id('d-colorpicker'));
        h2.getText().then(text => {
          expect(text).toEqual('Enhance the color');
        });
        let slidersMod: ElementArrayFinder = element.all(by.className('range-knob-handle'));
        slidersMod.count().then((data) => {
          expect(data).toEqual(3);
        });
        // move the ligthen slider to 25%
        setSliderPos(slidersMod.get(0), 25);
        browser.driver.sleep(1000); // wait for the animation
        let cols: ElementArrayFinder = element.all(by.css('ion-col'));
        cols.get(2).getText().then((data) => {
          let hexcolor: string = '#' + data.split('#')[1];
          expect(hexcolor).toEqual('#3f7fbf');
        });
        cols.get(3).getText().then((data) => {
          let hexselcolor: string = '#' + data.split('#')[1];
          expect(hexselcolor).toEqual('#9ebfdf');
        });
      });
    });
  });

  it('should get 25%R 50%G 75%B brighten 15% color', () => {
    let button: ElementFinder = element(by.id('b-colorpicker'));
    button.click().then(() => {
      browser.driver.sleep(1000); // wait for the animation
      let sliders: ElementArrayFinder = element.all(by.className('range-knob-handle'));
      sliders.count().then((data) => {
        expect(data).toEqual(3);
      });
      // move the red slider to 25%
      setSliderPos(sliders.get(0), 100 - 25);
      browser.driver.sleep(1000); // wait for the animation
      // move the green slider to 50%
      setSliderPos(sliders.get(1), 100 - 50);
      browser.driver.sleep(1000); // wait for the animation
      // move the green slider to 75%
      setSliderPos(sliders.get(2), 100 - 75);
      browser.driver.sleep(1000); // wait for the animation
      let buttonModif: ElementFinder = element(by.id('modif'));
      buttonModif.click().then(() => {
        browser.driver.sleep(1000); // wait for the animation
        let h2: ElementFinder = element(by.id('d-colorpicker'));
        h2.getText().then(text => {
          expect(text).toEqual('Enhance the color');
        });
        let slidersMod: ElementArrayFinder = element.all(by.className('range-knob-handle'));
        slidersMod.count().then((data) => {
          expect(data).toEqual(3);
        });
        // move the brighten slider to 15%
        setSliderPos(slidersMod.get(1), 15);
        browser.driver.sleep(1000); // wait for the animation
        let cols: ElementArrayFinder = element.all(by.css('ion-col'));
        cols.get(2).getText().then((data) => {
          let hexcolor: string = '#' + data.split('#')[1];
          expect(hexcolor).toEqual('#3f7fbf');
        });
        cols.get(3).getText().then((data) => {
          let hexselcolor: string = '#' + data.split('#')[1];
          expect(hexselcolor).toEqual('#65a5e5');
        });
      });
    });
  });

  it('should get 25%R 50%G 75%B darken 20% color', () => {
    let button: ElementFinder = element(by.id('b-colorpicker'));
    button.click().then(() => {
      browser.driver.sleep(1000); // wait for the animation
      let sliders: ElementArrayFinder = element.all(by.className('range-knob-handle'));
      sliders.count().then((data) => {
        expect(data).toEqual(3);
      });
      // move the red slider to 25%
      setSliderPos(sliders.get(0), 100 - 25);
      browser.driver.sleep(1000); // wait for the animation
      // move the green slider to 50%
      setSliderPos(sliders.get(1), 100 - 50);
      browser.driver.sleep(1000); // wait for the animation
      // move the green slider to 75%
      setSliderPos(sliders.get(2), 100 - 75);
      browser.driver.sleep(1000); // wait for the animation
      let buttonModif: ElementFinder = element(by.id('modif'));
      buttonModif.click().then(() => {
        browser.driver.sleep(1000); // wait for the animation
        let h2: ElementFinder = element(by.id('d-colorpicker'));
        h2.getText().then(text => {
          expect(text).toEqual('Enhance the color');
        });
        let slidersMod: ElementArrayFinder = element.all(by.className('range-knob-handle'));
        slidersMod.count().then((data) => {
          expect(data).toEqual(3);
        });
        // move the darken slider to 20%
        setSliderPos(slidersMod.get(2), 20);
        browser.driver.sleep(1000); // wait for the animation
        let cols: ElementArrayFinder = element.all(by.css('ion-col'));
        cols.get(2).getText().then((data) => {
          let hexcolor: string = '#' + data.split('#')[1];
          expect(hexcolor).toEqual('#3f7fbf');
        });
        cols.get(3).getText().then((data) => {
          let hexselcolor: string = '#' + data.split('#')[1];
          expect(hexselcolor).toEqual('#264c72');
        });
      });
    });
  });

  it('should get 25%R 50%G 75%B L25% B15% D20% color', () => {
    let button: ElementFinder = element(by.id('b-colorpicker'));
    button.click().then(() => {
      browser.driver.sleep(1000); // wait for the animation
      let sliders: ElementArrayFinder = element.all(by.className('range-knob-handle'));
      sliders.count().then((data) => {
        expect(data).toEqual(3);
      });
      // move the red slider to 25%
      setSliderPos(sliders.get(0), 100 - 25);
      browser.driver.sleep(1000); // wait for the animation
      // move the green slider to 50%
      setSliderPos(sliders.get(1), 100 - 50);
      browser.driver.sleep(1000); // wait for the animation
      // move the green slider to 75%
      setSliderPos(sliders.get(2), 100 - 75);
      browser.driver.sleep(1000); // wait for the animation
      let buttonModif: ElementFinder = element(by.id('modif'));
      buttonModif.click().then(() => {
        browser.driver.sleep(1000); // wait for the animation
        let h2: ElementFinder = element(by.id('d-colorpicker'));
        h2.getText().then(text => {
          expect(text).toEqual('Enhance the color');
        });
        let slidersMod: ElementArrayFinder = element.all(by.className('range-knob-handle'));
        slidersMod.count().then((data) => {
          expect(data).toEqual(3);
        });
        // move the ligthen slider to 25%
        setSliderPos(slidersMod.get(0), 25);
        browser.driver.sleep(1000); // wait for the animation
        // move the brighten slider to 15%
        setSliderPos(slidersMod.get(1), 15);
        browser.driver.sleep(1000); // wait for the animation
        // move the darken slider to 20%
        setSliderPos(slidersMod.get(2), 20);
        browser.driver.sleep(1000); // wait for the animation
        let cols: ElementArrayFinder = element.all(by.css('ion-col'));
        cols.get(2).getText().then((data) => {
          let hexcolor: string = '#' + data.split('#')[1];
          expect(hexcolor).toEqual('#3f7fbf');
        });
        cols.get(3).getText().then((data) => {
          let hexselcolor: string = '#' + data.split('#')[1];
          expect(hexselcolor).toEqual('#78b2ec');
        });
      });
    });
  });

  it('should get 25%R 50%G 75%B color after Reset', () => {
    let button: ElementFinder = element(by.id('b-colorpicker'));
    button.click().then(() => {
      browser.driver.sleep(1000); // wait for the animation
      let sliders: ElementArrayFinder = element.all(by.className('range-knob-handle'));
      sliders.count().then((data) => {
        expect(data).toEqual(3);
      });
      // move the red slider to 25%
      setSliderPos(sliders.get(0), 100 - 25);
      browser.driver.sleep(1000); // wait for the animation
      // move the green slider to 50%
      setSliderPos(sliders.get(1), 100 - 50);
      browser.driver.sleep(1000); // wait for the animation
      // move the green slider to 75%
      setSliderPos(sliders.get(2), 100 - 75);
      browser.driver.sleep(1000); // wait for the animation
      let buttonModif: ElementFinder = element(by.id('modif'));
      buttonModif.click().then(() => {
        browser.driver.sleep(1000); // wait for the animation
        let h2: ElementFinder = element(by.id('d-colorpicker'));
        h2.getText().then(text => {
          expect(text).toEqual('Enhance the color');
        });
        let slidersMod: ElementArrayFinder = element.all(by.className('range-knob-handle'));
        slidersMod.count().then((data) => {
          expect(data).toEqual(3);
        });
        // move the ligthen slider to 25%
        setSliderPos(slidersMod.get(0), 25);
        browser.driver.sleep(1000); // wait for the animation
        // move the brighten slider to 15%
        setSliderPos(slidersMod.get(1), 15);
        browser.driver.sleep(1000); // wait for the animation
        // move the darken slider to 20%
        setSliderPos(slidersMod.get(2), 20);
        browser.driver.sleep(1000); // wait for the animation
        let cols: ElementArrayFinder = element.all(by.css('ion-col'));
        cols.get(2).getText().then((data) => {
          let hexcolor: string = '#' + data.split('#')[1];
          expect(hexcolor).toEqual('#3f7fbf');
        });
        cols.get(3).getText().then((data) => {
          let hexselcolor: string = '#' + data.split('#')[1];
          expect(hexselcolor).toEqual('#78b2ec');
        });
        let buttonReset: ElementFinder = element(by.id('reset'));
        buttonReset.click().then(() => {
          browser.driver.sleep(1000); // wait for the animation
          cols.get(2).getText().then((data) => {
            let hexcolor: string = '#' + data.split('#')[1];
            expect(hexcolor).toEqual('#3f7fbf');
          });
          cols.get(3).getText().then((data) => {
            let hexselcolor: string = '#' + data.split('#')[1];
            expect(hexselcolor).toEqual('#3f7fbf');
          });
        });
      });
    });
  });

  it('should get back to Color button when pressing Ok on Enhance Color', () => {
    let button: ElementFinder = element(by.id('b-colorpicker'));
    button.click().then(() => {
      browser.driver.sleep(1000); // wait for the animation
      let sliders: ElementArrayFinder = element.all(by.className('range-knob-handle'));
      sliders.count().then((data) => {
        expect(data).toEqual(3);
      });
      // move the red slider to 25%
      setSliderPos(sliders.get(0), 100 - 25);
      browser.driver.sleep(1000); // wait for the animation
      // move the green slider to 50%
      setSliderPos(sliders.get(1), 100 - 50);
      browser.driver.sleep(1000); // wait for the animation
      // move the green slider to 75%
      setSliderPos(sliders.get(2), 100 - 75);
      browser.driver.sleep(1000); // wait for the animation
      let buttonModif: ElementFinder = element(by.id('modif'));
      buttonModif.click().then(() => {
        browser.driver.sleep(1000); // wait for the animation
        let h2: ElementFinder = element(by.id('d-colorpicker'));
        h2.getText().then(text => {
          expect(text).toEqual('Enhance the color');
        });
        let slidersMod: ElementArrayFinder = element.all(by.className('range-knob-handle'));
        slidersMod.count().then((data) => {
          expect(data).toEqual(3);
        });
        // move the ligthen slider to 25%
        setSliderPos(slidersMod.get(0), 25);
        browser.driver.sleep(1000); // wait for the animation
        // move the brighten slider to 15%
        setSliderPos(slidersMod.get(1), 15);
        browser.driver.sleep(1000); // wait for the animation
        // move the darken slider to 20%
        setSliderPos(slidersMod.get(2), 20);
        browser.driver.sleep(1000); // wait for the animation
        let cols: ElementArrayFinder = element.all(by.css('ion-col'));
        cols.get(2).getText().then((data) => {
          let hexcolor: string = '#' + data.split('#')[1];
          expect(hexcolor).toEqual('#3f7fbf');
        });
        cols.get(3).getText().then((data) => {
          let hexselcolor: string = '#' + data.split('#')[1];
          expect(hexselcolor).toEqual('#78b2ec');
        });
        let buttonReset: ElementFinder = element(by.id('reset'));
        buttonReset.click().then(() => {
          browser.driver.sleep(1000); // wait for the animation
          cols.get(2).getText().then((data) => {
            let hexcolor: string = '#' + data.split('#')[1];
            expect(hexcolor).toEqual('#3f7fbf');
          });
          cols.get(3).getText().then((data) => {
            let hexselcolor: string = '#' + data.split('#')[1];
            expect(hexselcolor).toEqual('#3f7fbf');
            let buttonValid: ElementFinder = element(by.id('valid'));
            buttonValid.click().then(() => {
              browser.driver.sleep(1000); // wait for the animation
              let buttonColor: ElementFinder = element(by.id('b-colorpicker'));
              browser.isElementPresent(buttonColor).then(value => {
                expect(value).toBe(true);
                buttonColor.getText().then(text => {
                  expect(text).toEqual('Color');
                });
              });
            });
          });
        });
      });
    });
  });
  it('should get back to Choose a color  when pressing Ok on Enhance the color', () => {
    let button: ElementFinder = element(by.id('b-colorpicker'));
    button.click().then(() => {
      browser.driver.sleep(1000); // wait for the animation
      let sliders: ElementArrayFinder = element.all(by.className('range-knob-handle'));
      sliders.count().then((data) => {
        expect(data).toEqual(3);
      });
      // move the red slider to 25%
      setSliderPos(sliders.get(0), 100 - 25);
      browser.driver.sleep(1000); // wait for the animation
      // move the green slider to 50%
      setSliderPos(sliders.get(1), 100 - 50);
      browser.driver.sleep(1000); // wait for the animation
      // move the green slider to 75%
      setSliderPos(sliders.get(2), 100 - 75);
      browser.driver.sleep(1000); // wait for the animation
      let buttonModif: ElementFinder = element(by.id('modif'));
      buttonModif.click().then(() => {
        browser.driver.sleep(1000); // wait for the animation
        let h2: ElementFinder = element(by.id('d-colorpicker'));
        h2.getText().then(text => {
          expect(text).toEqual('Enhance the color');
        });
        let slidersMod: ElementArrayFinder = element.all(by.className('range-knob-handle'));
        slidersMod.count().then((data) => {
          expect(data).toEqual(3);
        });
        // move the ligthen slider to 25%
        setSliderPos(slidersMod.get(0), 25);
        browser.driver.sleep(1000); // wait for the animation
        // move the brighten slider to 15%
        setSliderPos(slidersMod.get(1), 15);
        browser.driver.sleep(1000); // wait for the animation
        // move the darken slider to 20%
        setSliderPos(slidersMod.get(2), 20);
        browser.driver.sleep(1000); // wait for the animation
        let cols: ElementArrayFinder = element.all(by.css('ion-col'));
        cols.get(2).getText().then((data) => {
          let hexcolor: string = '#' + data.split('#')[1];
          expect(hexcolor).toEqual('#3f7fbf');
        });
        cols.get(3).getText().then((data) => {
          let hexselcolor: string = '#' + data.split('#')[1];
          expect(hexselcolor).toEqual('#78b2ec');
        });
        let buttonReset: ElementFinder = element(by.id('reset'));
        buttonReset.click().then(() => {
          browser.driver.sleep(1000); // wait for the animation
          cols.get(2).getText().then((data) => {
            let hexcolor: string = '#' + data.split('#')[1];
            expect(hexcolor).toEqual('#3f7fbf');
          });
          cols.get(3).getText().then((data) => {
            let hexselcolor: string = '#' + data.split('#')[1];
            expect(hexselcolor).toEqual('#3f7fbf');
            let buttonColor: ElementFinder = element(by.id('color'));
            buttonColor.click().then(() => {
              browser.driver.sleep(1000); // wait for the animation
              let h2c: ElementFinder = element(by.id('d-colorpicker'));
              h2c.getText().then(text => {
                expect(text).toEqual('Choose a color');
              });
            });
          });
        });
      });
    });
  });

});


function setSliderPos(slider: ElementFinder, percent: number): void  {
  let pix: number;
  getSliderLength(slider).then((length) => {
    pix = Math.round(percent * length / 100);
    browser.actions().dragAndDrop(slider, {
        x: pix,
        y: 0,
    }).perform().then(() => {
      slider.getAttribute('aria-valuenow').then((value) => {
         // 1 pixel issue on Mac Math.round
        if(Number(value) === 128) {
          browser.actions().dragAndDrop(slider, {
              x: -1,
              y: 0,
          }).perform();
        }
      });
    });
  });

}

function getSliderLength(slider: ElementFinder): Promise<number> {
  return new Promise<number> ((resolve) => {
    let pix: number;
    let length: number;
    let range: number;
    let sign: number;
    slider.getAttribute('aria-valuemax').then((max) => {
      slider.getAttribute('aria-valuemin').then((min) => {
        slider.getAttribute('aria-valuenow').then((now) => {
          range = Number(max) - Number(min) ;
          if (now === max) {
            sign = - 1;
          } else {
            sign = 1;
          }
        // drag the slider of 155 pixels
          pix =  155 * sign;
          browser.actions().dragAndDrop(slider, {
              x: pix,
              y: 0,
          }).perform().then(() => {
            slider.getAttribute('aria-valuenow').then((value) => {
              if (sign === -1) {
                length = pix * Number(max) / (Number(max) - Number(value));
              } else {
                length = pix * Number(max) / Number(value);
              }
              browser.actions().dragAndDrop(slider, {
                  x: -pix,
                  y: 0,
              }).perform().then(() => {
                resolve (length);
              });
            });
          });
        });
      });
    });
  });
}
