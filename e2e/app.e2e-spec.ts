import { browser } from 'protractor';

describe('MyApp', () => {

  beforeEach(() => {
    browser.get('');
  });

  it('should have a title saying Home', () => {
    browser.getTitle().then(title => {
        expect(title).toEqual('Home');
      });
  });

});
