import { browser, element, by, ElementFinder } from 'protractor';

describe('Home', () => {

  beforeEach(() => {
    browser.get('');
  });
  it('should have a title ', () => {
    let home: ElementFinder = element(by.css('ion-title'));
    browser.isElementPresent(home).then(value => {
      expect(value).toBe(true);
    });
    home.getText().then(value => {
      expect(value).toEqual('Home');
    })
  });

  it('should have {nav}', () => {
    element(by.css('ion-navbar')).isPresent().then(value => {
    expect(value).toEqual(true);
    });
  });

  it('should have correct nav text for Home', () => {
    element(by.css('ion-navbar:first-child')).getText().then(value => {
    expect(value).toContain('Home');
    });
  });

  it('should have a colorPicker component', () => {
    let colorPicker: ElementFinder = element.all(by.css('color-picker')).first();
    browser.isElementPresent(colorPicker).then(value => {
      expect(value).toBe(true);
    });
  });

});
