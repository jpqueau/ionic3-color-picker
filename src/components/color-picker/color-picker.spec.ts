import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { DebugElement, Renderer } from '@angular/core';
import { IonicModule, Platform } from 'ionic-angular/index';
import { PlatformMock, MockRenderer } from '../../../test-config/mocks-ionic';
import { ColorPickerComponent } from './color-picker';

describe('ColorPickerComponent', function() {
  let de: DebugElement;
  let comp: ColorPickerComponent;
  let fixture: ComponentFixture<ColorPickerComponent>;
  let nativeElement: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ColorPickerComponent],
      imports: [
        IonicModule.forRoot(ColorPickerComponent)
      ],
      providers: [
        { provide: Platform, useClass: PlatformMock},
        { provide: Renderer, useClass: MockRenderer}
      ]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColorPickerComponent);
    comp = fixture.componentInstance;
    nativeElement = fixture.nativeElement;
    fixture.detectChanges();
  });

  afterEach(() => {
      fixture.destroy();
      comp = null;
      de = null;
      nativeElement = null;
  });

  it('initialises', () => {
    expect(comp).toBeTruthy();
    expect(nativeElement.querySelector('#b-colorpicker') === null).toBe(false);
    expect(nativeElement.querySelector('#d-colorpicker') === null).toBe(true);
    expect(comp.isColorPicker).toBe(false);
    expect(comp.isColorModify).toBe(false);
    expect(comp.rgbColor.r).toBe(255);
    expect(comp.rgbColor.g).toBe(255);
    expect(comp.rgbColor.b).toBe(255);
    expect(comp.hexSelColor).toBe('#ffffff');
    expect(comp.rgbdiv).toBeTruthy();
    expect(comp.findiv).toBeTruthy();
    expect(comp.lighten).toBe(0);
    expect(comp.brighten).toBe(0);
    expect(comp.darken).toBe(0);
  });

  it('should open the ColorPicker from the color button', fakeAsync (() => {

    spyOn(comp, 'openColorPicker').and.callThrough();
    nativeElement.querySelector('#b-colorpicker').click();
    fixture.detectChanges();
    tick();

    expect(comp.openColorPicker).toHaveBeenCalled();
    expect(comp.isColorPicker).toBe(true);
    expect(nativeElement.querySelector('#b-colorpicker') === null).toBe(true);
    expect(nativeElement.querySelector('#d-colorpicker') === null).toBe(false);
    expect(comp.divWidth).toBe(100);
    expect(comp.divHeight).toBe(100);
  }));
  it('should get the red color from internal state', fakeAsync (() => {
    comp.rgbColor = {r: 255, g: 0, b: 0};
    let redColor: string = comp.rgbStyle();
    expect(redColor).toBe('#ff0000');
    expect(comp.hexSelColor).toBe('#ff0000');
  }));

  it('should get the green color from internal state', fakeAsync (() => {
    comp.rgbColor = {r: 0, g: 255, b: 0};
    let greenColor: string = comp.rgbStyle();
    expect(greenColor).toBe('#00ff00');
    expect(comp.hexSelColor).toBe('#00ff00');
  }));

  it('should get the blue color from internal state', fakeAsync (() => {
    comp.rgbColor = {r: 0, g: 0, b: 255};
    let blueColor: string = comp.rgbStyle();
    expect(blueColor).toBe('#0000ff');
    expect(comp.hexSelColor).toBe('#0000ff');
  }));

  it('should get the dark color from internal state', fakeAsync (() => {
    comp.rgbColor = {r: 0, g: 0, b: 0};
    let blackColor: string = comp.rgbStyle();
    expect(blackColor).toBe('#000000');
    expect(comp.hexSelColor).toBe('#000000');
  }));

  it('should get the light grey color from internal state', fakeAsync (() => {
    comp.rgbColor = {r: 200, g: 200, b: 200};
    let blackColor: string = comp.rgbStyle();
    expect(blackColor).toBe('#c8c8c8');
    expect(comp.hexSelColor).toBe('#c8c8c8');
  }));

  it('should get the selected color lighten 10%', fakeAsync (() => {
    comp.rgbColor = {r: 100, g: 200, b: 150};
    let color: string = comp.rgbStyle();
    comp.lighten = 10;
    let mcolor: string = comp.finStyle();
    expect(color).toBe('#64c896');
    expect(mcolor).toBe('#8ad5af');
    expect(comp.hexSelColor).toBe('#8ad5af');
  }));

  it('should get the selected color brighten 15%', fakeAsync (() => {
    comp.rgbColor = {r: 100, g: 200, b: 150};
    let color: string = comp.rgbStyle();
    comp.brighten = 15;
    let mcolor: string = comp.finStyle();
    expect(color).toBe('#64c896');
    expect(mcolor).toBe('#8aeebc');
    expect(comp.hexSelColor).toBe('#8aeebc');
  }));

  it('should get the selected color darken 25%', fakeAsync (() => {
    comp.rgbColor = {r: 100, g: 200, b: 150};
    let color: string = comp.rgbStyle();
    comp.darken = 25;
    let mcolor: string = comp.finStyle();
    expect(color).toBe('#64c896');
    expect(mcolor).toBe('#2d7f56');
    expect(comp.hexSelColor).toBe('#2d7f56');
   }));

  it('should get the selected color lighten 20% brighten 15% darken 25%', fakeAsync (() => {
    comp.rgbColor = {r: 100, g: 200, b: 150};
    let color: string = comp.rgbStyle();
    comp.lighten = 20;
    comp.brighten = 15;
    comp.darken = 25;
    let mcolor: string = comp.finStyle();
    expect(color).toBe('#64c896');
    expect(mcolor).toBe('#77e7af');
    expect(comp.hexSelColor).toBe('#77e7af');
  }));

  it('emitting the red color from internal state', fakeAsync (() => {
    spyOn(comp, 'openColorPicker').and.callThrough();
   // Open the Color Picker
    nativeElement.querySelector('#b-colorpicker').click();
    fixture.detectChanges();
    tick();
    expect(comp.openColorPicker).toHaveBeenCalled();
    expect(comp.isColorPicker).toBe(true);
    // Set the Red color
    comp.rgbColor = {r: 255, g: 0, b: 0};
    let redColor: string = comp.rgbStyle();
    expect(redColor).toBe('#ff0000');
    expect(comp.hexSelColor).toBe('#ff0000');
    // Emit the Red Color
    let selColor: string;
    comp.colorSelected.subscribe((value) => selColor = value);
    comp.setSelection();
    expect(selColor).toBe('#ff0000');
  }));

  it('emitting the the selected RGB color from internal state', fakeAsync (() => {
    spyOn(comp, 'openColorPicker').and.callThrough();
   // Open the Color Picker
    nativeElement.querySelector('#b-colorpicker').click();
    fixture.detectChanges();
    tick();
    expect(comp.openColorPicker).toHaveBeenCalled();
    expect(comp.isColorPicker).toBe(true);
    expect(comp.isColorModify).toBe(false);
    // set the RGB color
    comp.rgbColor = {r: 100, g: 200, b: 150};
    let color: string = comp.rgbStyle();
    expect(color).toBe('#64c896');
    expect(comp.hexSelColor).toBe('#64c896');
    // emit the RGB color
    let selColor: string;
    comp.colorSelected.subscribe((value) => selColor = value);
    comp.setSelection();
    expect(selColor).toBe('#64c896');
  }));

  it('emitting the the selected Enhance color from internal state', fakeAsync (() => {
    spyOn(comp, 'openColorPicker').and.callThrough();
    spyOn(comp, 'setModification').and.callThrough();
   // Open the Color Picker
    nativeElement.querySelector('#b-colorpicker').click();
    fixture.detectChanges();
    tick();
    expect(comp.openColorPicker).toHaveBeenCalled();
    expect(comp.isColorPicker).toBe(true);
    expect(comp.isColorModify).toBe(false);
    // set the RGB color
    comp.rgbColor = {r: 100, g: 200, b: 150};
    let color: string = comp.rgbStyle();
    expect(color).toBe('#64c896');
    // Enhance the color
    nativeElement.querySelector('#modif').click();
    fixture.detectChanges();
    tick();
    expect(comp.setModification).toHaveBeenCalled();
    expect(comp.isColorPicker).toBe(true);
    expect(comp.isColorModify).toBe(true);
    // Set the Enhance color
    comp.lighten = 20;
    comp.brighten = 15;
    comp.darken = 25;
    let mcolor: string = comp.finStyle();
    expect(mcolor).toBe('#77e7af');
    expect(comp.hexSelColor).toBe('#77e7af');
    // emit the Enhance color
    let selColor: string;
    comp.colorSelected.subscribe((value) => selColor = value);
    comp.setSelection();
    expect(selColor).toBe('#77e7af');
  }));

  it('should emit the selected RGB color on OK click()', fakeAsync ( () => {
    spyOn(comp, 'openColorPicker').and.callThrough();
    spyOn(comp, 'setSelection').and.callThrough();
    spyOn(comp.colorSelected, 'emit').and.callThrough();
    // Open the Color Picker
    nativeElement.querySelector('#b-colorpicker').click();
    fixture.detectChanges();
    tick();
    expect(comp.openColorPicker).toHaveBeenCalled();
    expect(nativeElement.querySelector('#b-colorpicker') === null).toBe(true);
    expect(nativeElement.querySelector('#d-colorpicker') === null).toBe(false);
    expect(comp.isColorPicker).toBe(true);
    comp.rgbColor = {r: 100, g: 200, b: 150};
    let redColor: string = comp.rgbStyle();
    expect(redColor).toBe('#64c896');
    expect(comp.hexSelColor).toBe('#64c896');
    // Validate the RGB color
    nativeElement.querySelector('#valid').click();
    fixture.detectChanges();
    tick();
    expect(comp.setSelection).toHaveBeenCalled();
    expect(comp.colorSelected.emit).toHaveBeenCalledWith('#64c896');
  }));

  it('should emit the selected Enhance color on OK click()', fakeAsync ( () => {
    spyOn(comp, 'openColorPicker').and.callThrough();
    spyOn(comp, 'setModification').and.callThrough();
    spyOn(comp, 'setSelection').and.callThrough();
    spyOn(comp.colorSelected, 'emit').and.callThrough();
    // Open the Color Picker
    nativeElement.querySelector('#b-colorpicker').click();
    fixture.detectChanges();
    tick();
    expect(comp.openColorPicker).toHaveBeenCalled();
    expect(comp.isColorPicker).toBe(true);
    expect(comp.isColorModify).toBe(false);
    expect(nativeElement.querySelector('#b-colorpicker') === null).toBe(true);
    expect(nativeElement.querySelector('#d-colorpicker') === null).toBe(false);
    expect(comp.isColorPicker).toBe(true);
    comp.rgbColor = {r: 100, g: 200, b: 150};
    let redColor: string = comp.rgbStyle();
    expect(redColor).toBe('#64c896');
    // Enhance the color
    nativeElement.querySelector('#modif').click();
    fixture.detectChanges();
    tick();
    expect(comp.setModification).toHaveBeenCalled();
    expect(comp.isColorPicker).toBe(true);
    expect(comp.isColorModify).toBe(true);
    comp.lighten = 20;
    comp.brighten = 15;
    comp.darken = 25;
    let mcolor: string = comp.finStyle();
    expect(mcolor).toBe('#77e7af');
    expect(comp.hexSelColor).toBe('#77e7af');
    // Validate the selected color
    nativeElement.querySelector('#valid').click();
    fixture.detectChanges();
    tick();
    expect(comp.setSelection).toHaveBeenCalled();
    expect(comp.colorSelected.emit).toHaveBeenCalledWith('#77e7af');
  }));

  it('should reset the selected Enhance color on Reset click()', fakeAsync ( () => {
    spyOn(comp, 'openColorPicker').and.callThrough();
    spyOn(comp, 'setModification').and.callThrough();
    spyOn(comp, 'resetModification').and.callThrough();
    // Open the Color Picker
    nativeElement.querySelector('#b-colorpicker').click();
    fixture.detectChanges();
    tick();
    expect(comp.openColorPicker).toHaveBeenCalled();
    expect(comp.isColorPicker).toBe(true);
    expect(comp.isColorModify).toBe(false);
    expect(nativeElement.querySelector('#b-colorpicker') === null).toBe(true);
    expect(nativeElement.querySelector('#d-colorpicker') === null).toBe(false);
    expect(comp.isColorPicker).toBe(true);
    comp.rgbColor = {r: 100, g: 200, b: 150};
    let redColor: string = comp.rgbStyle();
    expect(redColor).toBe('#64c896');
    // Enhance the color
    nativeElement.querySelector('#modif').click();
    fixture.detectChanges();
    tick();
    expect(comp.setModification).toHaveBeenCalled();
    expect(comp.isColorPicker).toBe(true);
    expect(comp.isColorModify).toBe(true);
    comp.lighten = 20;
    comp.brighten = 15;
    comp.darken = 25;
    let mcolor: string = comp.finStyle();
    expect(mcolor).toBe('#77e7af');
    expect(comp.hexSelColor).toBe('#77e7af');
    // Reset the Enhance color
    nativeElement.querySelector('#reset').click();
    fixture.detectChanges();
    tick();
    expect(comp.resetModification).toHaveBeenCalled();
    expect(comp.lighten).toBe(0);
    expect(comp.brighten).toBe(0);
    expect(comp.darken).toBe(0);
    expect(comp.hexSelColor).toBe('#64c896');
  }));

  it('should go back to RGB color on Color click()', fakeAsync ( () => {
    spyOn(comp, 'openColorPicker').and.callThrough();
    spyOn(comp, 'setModification').and.callThrough();
    spyOn(comp, 'newColor').and.callThrough();
    spyOn(comp, 'resetModification').and.callThrough();
    // Open the Color Picker
    nativeElement.querySelector('#b-colorpicker').click();
    fixture.detectChanges();
    tick();
    expect(comp.openColorPicker).toHaveBeenCalled();
    expect(comp.isColorPicker).toBe(true);
    expect(comp.isColorModify).toBe(false);
    expect(nativeElement.querySelector('#b-colorpicker') === null).toBe(true);
    expect(nativeElement.querySelector('#d-colorpicker') === null).toBe(false);
    expect(comp.isColorPicker).toBe(true);
    comp.rgbColor = {r: 100, g: 200, b: 150};
    let redColor: string = comp.rgbStyle();
    expect(redColor).toBe('#64c896');
   // Enhance the color
    nativeElement.querySelector('#modif').click();
    fixture.detectChanges();
    tick();
    expect(comp.setModification).toHaveBeenCalled();
    expect(comp.isColorPicker).toBe(true);
    expect(comp.isColorModify).toBe(true);
    comp.lighten = 20;
    comp.brighten = 15;
    comp.darken = 25;
    let mcolor: string = comp.finStyle();
    expect(mcolor).toBe('#77e7af');
    expect(comp.hexSelColor).toBe('#77e7af');
   // Reset the Enhance color and go back to RGB color Selection
    nativeElement.querySelector('#color').click();
    fixture.detectChanges();
    tick();
    expect(comp.isColorModify).toBe(false);
    expect(comp.resetModification).toHaveBeenCalled();
    expect(comp.lighten).toBe(0);
    expect(comp.brighten).toBe(0);
    expect(comp.darken).toBe(0);
    expect(comp.hexSelColor).toBe('#64c896');
  }));

});
