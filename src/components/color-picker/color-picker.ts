import { Component, ViewChild, Renderer, Output, EventEmitter } from '@angular/core';
import { IonicPage, Platform } from 'ionic-angular';
import * as tinycolor from 'tinycolor2';

/**
 * Generated class for the ColorPickerComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage()
@Component({
  selector: 'color-picker',
  templateUrl: 'color-picker.html'
})
export class ColorPickerComponent {
  @ViewChild('rgbDiv') public rgbdiv: any;
  @ViewChild('finDiv') public findiv: any;
  @Output() public colorSelected: EventEmitter<string> = new EventEmitter();
  public rgbElement: any;
  public finElement: any;
  public isColorPicker: boolean;
  public isColorModify: boolean;
  public divWidth: number;
  public divHeight: number;
  public rgbColor: ColorFormats.RGB;
  public lighten: number;
  public brighten: number;
  public darken: number;
  public selColor: tinycolorInstance;
  public platform: Platform;
  public render: Renderer;
  public hexRGB: string;
  public hexSelColor: string;
  constructor(platform: Platform, render: Renderer) {
    this.platform = platform;
    this.render = render;
    this.initialize();
  }

  // tslint:disable-next-line:member-access
  ngAfterViewInit(): void {
    this.rgbElement = this.rgbdiv.nativeElement;
    this.finElement = this.findiv.nativeElement;
    this.render.setElementStyle(this.rgbElement, 'width', this.divWidth.toString() + '%');
    this.render.setElementStyle(this.rgbElement, 'height', this.divHeight.toString() + 'px');
    this.render.setElementStyle(this.finElement, 'width', this.divWidth.toString() + '%');
    this.render.setElementStyle(this.finElement, 'height', this.divHeight.toString() + 'px');
  }

  public openColorPicker(): void {
    this.isColorPicker = true;
    this.divWidth = 100;
    this.divHeight = 100;

    this.render.setElementStyle(this.rgbElement, 'width', this.divWidth.toString() + '%');
    this.render.setElementStyle(this.rgbElement, 'height', this.divHeight.toString() + 'px');
    this.render.setElementStyle(this.finElement, 'width', this.divWidth.toString() + '%');
    this.render.setElementStyle(this.finElement, 'height', this.divHeight.toString() + 'px');
  }

  public setSelection(): void {
    this.colorSelected.emit(this.selColor.toHexString());
    this.initialize();
    this.render.setElementStyle(this.rgbElement, 'width', this.divWidth.toString() + '%');
    this.render.setElementStyle(this.rgbElement, 'height', this.divHeight.toString() + 'px');
    this.render.setElementStyle(this.finElement, 'width', this.divWidth.toString() + '%');
    this.render.setElementStyle(this.finElement, 'height', this.divHeight.toString() + 'px');

  }

  public rgbStyle(): string {
    this.hexRGB = tinycolor(this.rgbColor).toHexString();
    this.selColor = tinycolor(this.rgbColor);
    this.hexSelColor = this.hexRGB;
    return this.hexRGB;
  }

  public finStyle(): string {
    this.selColor = tinycolor(this.rgbColor).lighten(this.lighten).darken(this.darken).brighten(this.brighten);
    this.hexSelColor = this.selColor.toHexString();
    return this.hexSelColor;
  }

  public setModification(): void {
     this.isColorModify = true;
  }

  public newColor(): void {
     this.isColorModify = false;
     this.resetModification();
  }

  public resetModification(): void {
    this.lighten = 0;
    this.brighten = 0;
    this.darken = 0;
    this.selColor = tinycolor(this.rgbColor);
    this.hexSelColor = this.selColor.toHexString();
  }

  private initialize(): void {
    this.isColorPicker = false;
    this.isColorModify = false;
    this.rgbColor = {r: 255, g: 255, b: 255};
    this.selColor =  tinycolor(this.rgbColor);
    this.hexRGB = tinycolor(this.rgbColor).toHexString();
    this.hexSelColor = this.selColor.toHexString();
    this.divWidth = 100;
    this.divHeight = 30;
    this.lighten = 0;
    this.brighten = 0;
    this.darken = 0;
  }

}
