import { async, TestBed } from '@angular/core/testing';
import { IonicModule, Platform } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { PlatformMock, StatusMock, SplashMock } from '../../test-config/mocks-ionic';

describe('MyApp Component', () => {

  let fixture;
  let component;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MyApp],
      imports: [
        IonicModule.forRoot(MyApp)
      ],
      providers: [

        { provide: StatusBar, useClass: StatusMock },
        { provide: SplashScreen, useClass: SplashMock },
        { provide: Platform, useClass: PlatformMock }
      ]
    })
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyApp);
    component = fixture.componentInstance;
  });


  it('should be created', () => {
    expect(component instanceof MyApp).toBe(true);
  });

  it('initialises with the root page Home', () => {
    expect(component['rootPage']).not.toBe(null);
    expect(component['rootPage']).toBe(HomePage);
  });

});
