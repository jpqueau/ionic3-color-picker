import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { IonicModule, NavController } from 'ionic-angular/index';
import {  NavMock } from '../../../test-config/mocks-ionic';
import { HomePage } from './home';
import { ColorPickerComponent } from '../../components/color-picker/color-picker';

describe('HomePage', function() {
  let de: DebugElement;
  let comp: HomePage;
  let fixture: ComponentFixture<HomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomePage, ColorPickerComponent],
      imports: [
        IonicModule.forRoot(HomePage)
      ],
      providers: [
        { provide: NavController, useClass: NavMock},
      ]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePage);
    comp = fixture.componentInstance;
  });

  afterEach(() => {
      fixture.destroy();
      comp = null;
      de = null;
  });

  it('should create component', () => expect(comp).toBeDefined());

  it('initialises', () => {
    expect(comp).toBeTruthy();
    de = fixture.debugElement.query(By.css('color-picker'));
    expect(de.nativeElement === null).toBe(false);
  });

  it('should get the color', () => {
    let color: string = '#ade7ff';
    comp.getColor(color);
    expect(comp.color).toEqual('#ade7ff');
  });

});
