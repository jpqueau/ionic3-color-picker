import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  public color: string;

  constructor(public navCtrl: NavController) {
  }

  public getColor(color: string): void {
    console.log(color);
    this.color = color;
  }

}
